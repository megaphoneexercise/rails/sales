module Rest
  module Exception
    class BadRequest < StandardError
    end
  end
end
