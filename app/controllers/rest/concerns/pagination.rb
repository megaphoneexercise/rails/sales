module Rest
  module Concerns
    module Pagination
      extend ActiveSupport::Concern
      included do
        protected

        delegate :next_url, :redirect_url, to: :request_url_generator

        def limit
          @limit ||= request.headers['X-Pagination-Param'] || params[:limit]&.to_i || 50
        end

        def models_full_results
          return @models_full_results unless @models_full_results.nil?
          @models_full_results = models.where(queryable_params)
          @models_full_results = @models_full_results.order(pagination_param_name => pagination_param_direction) unless pagination_param_name.nil?
          @models_full_results
        end

        def models_count
          @total = models_full_results.count
        end

        def models_remaining
          @models_remaining ||= models_full_results
          if pagination_param_name.nil?
            @models_remaining
          else
            @models_remaining = @models_remaining&.where("\"#{pagination_param_name}\" > ?", pagination_param_value) if pagination_param_value.present?
          end
        end

        def pagination_param_name
          @pagination_param_name ||= request.headers['X-Pagination-Param'] || params[:pagination_param]
        end

        def pagination_param_direction
          @pagination_param_direction ||= request.headers['X-Pagination-Direction'] || params[:pagination_direction] || :asc # from params
        end

        def pagination_param_value
          @pagination_param_value ||= params[pagination_param_name]
        end

        def next_url
          return if models_count == 0 || models_remaining.count <= limit
          "#{current_url}?#{new_params.to_param}"
        end

        def redirect_url(model)
          if action_name == 'update'
            current_url
          else
            "#{current_url}/#{model.id}"
          end
        end

        def new_params
          query_parameters.dup.merge(pagination_param_name.to_s => models_remaining.limit(limit).last.send(pagination_param_name))
        end

        def current_url
          "#{protocol}#{host_with_port}#{micro_service_prefix}#{path}"
        end

        def micro_service_prefix
          request.headers['HTTP_X_REAL_PATH'].sub(path, '') if request.headers['HTTP_X_REAL_PATH'].present?
        end

        def protocol
          return 'http://' if Rails.env.development?
          'https://'
        end
      end
    end
  end
end

