require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Budgets
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 6.0

    config.middleware.insert_before 0, Rack::Cors do
      allow do
        origins '*'
        resource '*',
                 headers: :any,
                 expose: ['Authorization'],
                 methods: [:get, :post, :options, :delete, :put]
      end
    end

    config.action_dispatch.default_headers = {
        'Access-Control-Allow-Methods' => %w{POST PUT DELETE GET OPTIONS}.join(","),
        'Access-Control-Allow-Origin' => '*',
        'Access-Control-Request-Method' => '*',
        'Access-Control-Allow-Headers' => '*',
        'Access-Control-Expose-Headers' => '*'
    }
  end
end
