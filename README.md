# README

To run:

* Clone this repo
* `bundle install`
* `rake db:create`
* `rake db:migrate`
* `bundle exec rails s`

Note that my config/database.yml file is set up for a postgres database and expects a role with the username/password of 'postgres'
