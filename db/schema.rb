# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_06_02_131834) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "uuid-ossp"

  create_table "categories", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.string "title"
    t.integer "total_balance"
    t.integer "total_budget"
    t.integer "total_available"
    t.boolean "archived", default: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["archived"], name: "index_categories_on_archived"
  end

  create_table "csvs", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.json "original_file"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "file_type"
  end

  create_table "sales", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.string "customer_name"
    t.string "item_description"
    t.float "item_price"
    t.integer "quantity"
    t.string "merchant_name"
    t.string "merchant_address"
    t.boolean "archived", default: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["archived"], name: "index_sales_on_archived"
  end

  create_table "subcategories", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.string "title"
    t.string "category_id"
    t.integer "balance"
    t.integer "budget"
    t.integer "available"
    t.boolean "archived", default: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["archived"], name: "index_subcategories_on_archived"
  end

end
