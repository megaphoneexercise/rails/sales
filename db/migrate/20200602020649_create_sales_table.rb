class CreateSalesTable < ActiveRecord::Migration[6.0]
  def change
    ActiveRecord::Base.connection.execute 'CREATE EXTENSION IF NOT EXISTS "uuid-ossp"'

    create_table :sales, id: :uuid, default: -> { 'uuid_generate_v4()' } do |t|
      t.string :customer_name
      t.string :item_description
      t.float :item_price
      t.integer :quantity
      t.string :merchant_name
      t.string :merchant_address
      t.boolean :archived, default: false, index: true

      t.timestamps
    end
  end
end
