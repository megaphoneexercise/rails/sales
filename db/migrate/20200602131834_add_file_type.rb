class AddFileType < ActiveRecord::Migration[6.0]
  def change
    add_column :csvs, :file_type, :string
  end
end
