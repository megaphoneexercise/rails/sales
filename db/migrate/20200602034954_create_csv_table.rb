class CreateCsvTable < ActiveRecord::Migration[6.0]
  def change
    create_table :csvs, id: :uuid, default: -> { 'uuid_generate_v4()' } do |t|
      t.json :original_file

      t.timestamps
    end
  end
end
